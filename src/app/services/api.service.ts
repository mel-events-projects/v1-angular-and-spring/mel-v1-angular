import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { EventModel } from '../models/event.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


/**
 * The ApiService allows to get data from the api opendata.lillemetropole.fr
 * It uses http protocol.
 */
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private apiUrlBasis = environment.apiUrlBasis;
  private globalParameters = environment.globalParameters;
  private exports = environment.exports;
  private apiUrlEnd = environment.apiUrlEnd;
  private apiKey = environment.apiKey;

  constructor(private http: HttpClient) { }

  /**
   * Gets all events from api, based on global research parameters mentioned in environment.ts
   */
  getEvents(): Observable<EventModel[]> {
    return this.http.get(`${this.apiUrlBasis}${this.exports}${this.globalParameters}${this.apiUrlEnd}${this.apiKey}`).pipe(
      map(
        (jsonArray: any) => jsonArray.map(jsonItem => EventModel.fromJson(jsonItem))
      )
    );
  }

  /**
   * Gets one event from api, by id.
   */
  getEventsById(id): Observable<EventModel> {
    return this.http.get(`${this.apiUrlBasis}${this.exports}?where=uid%3D%22${id}%22`).pipe(
      map((result: EventModel[]) => EventModel.fromJson(result[0]))
      );
  }
}


