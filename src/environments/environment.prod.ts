export const environment = {
  // Extern Api urls :
  apiKey: '', // Add your api key here (you must create an account on https://opendata.lillemetropole.fr/signup/)
  apiUrlBasis: 'https://opendata.lillemetropole.fr/api/v2/catalog/datasets/evenements-publics-openagenda/',
  exports: 'exports/json',
  globalParameters: '?where=date_start%3E%3Dnow()&sort=-date_start',
  apiUrlEnd: `&apikey=`,
  // Spring Api url :
  serverUrl: 'http://localhost:8090/',
};
