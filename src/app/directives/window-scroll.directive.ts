import { Directive, HostListener, ElementRef, HostBinding } from '@angular/core';

/**
 * Binds the class "top-fixed-position" regarding a scroll event and the window position inside the page.
 * This allows to change the SearchFormComponent position.
 */
@Directive({
  selector: '[cdcWindowScroll]'
})
export class WindowScrollDirective {
  
  private isScrolledDown: boolean = false;

  constructor(private element: ElementRef<HTMLInputElement>) { }

  /**
   * Binds the class "top-fixed-position" to the boolean isScrolledDown.
   */
  @HostBinding('class.top-fixed-position')
  get isTopFixedPosition() {
    return this.isScrolledDown;
  }

  /**
   * Listens to the 'scroll' event and tests the position of the window inside the page.
   */
  @HostListener('window:scroll', ['$event'])
  ScrollPosition(event) {
    if (window.pageYOffset > 90) {
      this.isScrolledDown = true;
    } else {
      this.isScrolledDown = false;
    }
  }

}
