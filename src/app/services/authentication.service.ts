import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';


export class User {
  constructor(
    public status: string,
  ) {}
}

export class jwtResponse {
  constructor(
    public jwtToken: string,
  ) {}
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private URLBasis = environment.serverUrl;
  private URLAuthenticate = "authenticate";
  
  constructor(private httpClient: HttpClient) { }

  authenticate(username, password) {
    return this.httpClient.post<any>(`${this.URLBasis}${this.URLAuthenticate}`, {username, password}).pipe(
      map(
        userData => {
          sessionStorage.setItem('username', username);
          let tokenStr = `Bearer ${userData.token}`;
          sessionStorage.setItem('token', tokenStr);
          return userData;
        }
      )
    );
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem('username');
    return !(user == null);
  }

  logOut() {
    sessionStorage.removeItem('username');
  }

  
}
