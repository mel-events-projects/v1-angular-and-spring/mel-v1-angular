import { environment } from './../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NewUser } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserApiService {

  private URLBasis = environment.serverUrl;
  private URLRegister = "register";
  constructor(private httpClient: HttpClient) { }

  registerUser(user: NewUser) {
    console.log(`${user.password} dans user-api.service`);
    return this.httpClient.post<any>(`${this.URLBasis}${this.URLRegister}`, user);
  }
  
}
