import { SearchFields } from './../../models/searchFields.model';
import { SearchFilterService } from './../../services/search-filter.service';
import { ApiService } from './../../services/api.service';
import { Component, OnInit } from '@angular/core';
import { EventModel } from 'src/app/models/event.model';
import { SortResultPipe } from 'src/app/sort-result.pipe';

/**
 * EventComponent gets a list of events and instatiate an EventThumbnailComponent for each event.
 */
@Component({
  selector: 'cdc-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {
  
  private eventsApi;
  private eventsList;
  private isLoaded: boolean = false;
  private today = new Date();
  private homePageFilters = new SearchFields('', '', this.today, new Date(this.today.getFullYear() + 1, this.today.getMonth(), this.today.getDate()))
  
  constructor(private sortResult: SortResultPipe, private apiService: ApiService, private searchFilterService: SearchFilterService) { 
  }

  ngOnInit() {
    this.apiService.getEvents().subscribe(
      (events) => {
        this.eventsApi = events;
        this.eventsList = this.sortResult.transform(this.searchFilterService.applyFilters(this.eventsApi, this.homePageFilters));
        this.isLoaded = true;
      }, err => console.log(err)
    )
  }

  /**
   * Apply research parameters to filter the events list.
   * @param filters SearchFields object with search parameters to apply
   */
  doResearch(filters: SearchFields) {
    this.eventsList = this.sortResult.transform(this.searchFilterService.applyFilters(this.eventsList, filters));
  }

  /**
   * Display the initial list of events (non filtered).
   */
  doReset() {
    this.eventsList = this.sortResult.transform(this.eventsApi);
  }

}
