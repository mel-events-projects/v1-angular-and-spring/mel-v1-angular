import { Component, OnInit, Input } from '@angular/core';
import { EventModel } from 'src/app/models/event.model';

/**
 * EventThumbnailComponent display a short presentation of an event.
 */
@Component({
  selector: 'cdc-event-thumbnail',
  templateUrl: './event.thumbnail.component.html',
  styleUrls: ['./event.thumbnail.component.scss']
})
export class EventThumbnailComponent implements OnInit {

  @Input()
  eventData: EventModel;
  
  private src: String;
  private loadImg: boolean = true;
  
  constructor() { }

  ngOnInit() {
    this.src = this.eventData.imageThumb;

  }

   /**
   * Hide image of the event if not available (loadImg=false)
   * @param event: error when image is loaded
   */
  dontShow(event) {
    this.loadImg = false;
  }

}
