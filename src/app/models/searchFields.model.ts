/**
 * This model regroups all search fields that can be used to filter the events list.
 */
export class SearchFields {
    keyword: string;
    city: string;
    periodBegin: Date;
    periodEnd: Date;

    constructor(keyword: string, city: string, periodBegin: Date, periodEnd: Date) {
        this.keyword = keyword;
        this.city = city;
        this.periodBegin = periodBegin;
        this.periodEnd = periodEnd;
    }
}