import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { ProgressSpinnerModule } from 'primeng/progressspinner';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EventsComponent } from './components/events/events.component';
import { EventThumbnailComponent } from './components/event.thumbnail/event.thumbnail.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { WindowScrollDirective } from './directives/window-scroll.directive';
import { EventDetailsComponent } from './components/event-details/event-details.component';
import { SortResultPipe } from './sort-result.pipe';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { LogoutButtonComponent } from './components/logout-button/logout-button.component';
import { RegisterFormComponent } from './components/register-form/register-form.component';
import { RegisterPageComponent } from './components/register-page/register-page.component';

@NgModule({
  declarations: [
    AppComponent,
    EventsComponent,
    EventThumbnailComponent,
    SearchFormComponent,
    WindowScrollDirective,
    EventDetailsComponent,
    SortResultPipe,
    LoginFormComponent,
    LoginPageComponent,
    LogoutButtonComponent,
    RegisterFormComponent,
    RegisterPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    InputTextModule,
    ButtonModule,
    ProgressSpinnerModule
  ],
  providers: [SortResultPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
