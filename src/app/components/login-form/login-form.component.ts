import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'cdc-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  private loginForm: FormGroup;
  private invalidLoginSent: boolean;

  constructor(private fb: FormBuilder, private authService: AuthenticationService, private router: Router) { 
    this.loginForm = fb.group({
      username: fb.control(''),
      password: fb.control('')
    });
  }

  ngOnInit() {
    this.invalidLoginSent = false;
  }

  cancel() {
    this.loginForm.setValue({
      username: '',
      password: ''
    });
    this.invalidLoginSent = false;
  }

  login() {
    this.authService.authenticate(this.loginForm.value.username, this.loginForm.value.password).subscribe(
      result => {
        this.invalidLoginSent = false;
        this.router.navigate(['']);
      }, err => {
        this.invalidLoginSent = true;
        console.log(err);
      }
    );
  }

}
