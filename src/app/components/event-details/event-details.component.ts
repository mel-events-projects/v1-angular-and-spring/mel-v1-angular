import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EventModel } from 'src/app/models/event.model';
import { ApiService } from 'src/app/services/api.service';

/**
 * EventDetailsComponent display a detailed presentation of an event.
 */
@Component({
  selector: 'cdc-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.scss']
})
export class EventDetailsComponent implements OnInit {
  
  private eventToDetail: EventModel;
  private src: String;
  private loadImg: boolean = true;
  private isLoaded: boolean = false;
  private uniqueDate: boolean = false;

  constructor(private apiService: ApiService, private route: ActivatedRoute) { }

  ngOnInit() {
    // Get eventId from the url parameters
    const eventId = this.route.snapshot.paramMap.get('eventId');

    // Get details of this event by an Http request to the api
    this.apiService.getEventsById(eventId).subscribe(
      (result) => {
        this.eventToDetail = result;
        this.src = this.eventToDetail.image;
        this.uniqueDate = this.eventToDetail.isOneDay(); //to show only one day if one day event
        this.isLoaded = true; // to show data once loaded only
        console.log(this.eventToDetail);
      }, err => console.log(err)
    );
  }

  /**
   * Hide image of the event if not available (loadImg=false)
   * @param event: error when image is loaded
   */
  dontShow(event) {
    this.loadImg = false;
  }

}
