import { Time } from '@angular/common';
import { TimetableSlot } from './timetableSlot.model';

/**
 * This class is the model for any event to be displayed in the application.
 * Its methods helps to format the event data from json to an EventModel object.
 */
export class EventModel {
    constructor(public address: string,
                public city: string,
                public dateEnd: Date,
                public dateStart: Date,
                public department: string,
                public description: string,
                public freeText: string,
                public image: string,
                public imageThumb: string,
                public lang: string,
                public latlon: Array<number>,
                public link: string,
                public placename: string,
                public region: string,
                public spaceTimeInfo: string,
                public timetable: Array<TimetableSlot>,
                public timetableThumb: Array<TimetableSlot | string>,
                public title: string,
                public uid: number,
                public updatedAt: Time) {
                }

    /**
     * Format the timetable fields from json to an array of timetableslots
     * @param timetableJson: the timetable fields from the json object
     */
    public static adjustTimetable(timetableJson: string): Array<TimetableSlot> {
        let timetableSlots : Array<TimetableSlot> = [];
        if (timetableJson) {
          let timetableStr = new String(timetableJson);            
          for (const slot of timetableStr.split(";")) {
            let timetableSlot = new TimetableSlot();
            timetableSlot.begin = new Date(slot.split(" ")[0]);
            timetableSlot.end = new Date(slot.split(" ")[1]);
            timetableSlots.push(timetableSlot);
          }
        }
        return timetableSlots;
    }
    
    /** 
     * Create a shorter array of timetableslots to show in the thumbnails
     * @param timetableSlots: the array of timetableslots from an EventModel Object
    */
    public static createTimetableThumb(timetableSlots: TimetableSlot[]): Array<TimetableSlot | string> {
        let timetableSlotsThumb: Array<TimetableSlot | string> = [];
        if (timetableSlots.length > 2) {
            timetableSlotsThumb = [timetableSlots[0], timetableSlots[1], '...'];
        } else {
        timetableSlotsThumb = timetableSlots;
        }
        return timetableSlotsThumb;
    }

    /**
     * Transform the json response in an EventModel object
     * @param json: the json object received from the api request.
     */
    public static fromJson(json: Object): EventModel {
        let timetableFromJson = this.adjustTimetable(json['timetable']);
        let eventFromJson =  new EventModel(
            json['address'],
            json['city'],
            new Date(json['date_end']),
            new Date(json['date_start']),
            json['department'],
            json['description'],
            json['free_text'],
            json['image'],
            json['image_thumb'],
            json['lang'],
            json['latlon'],
            json['link'],
            json['placename'],
            json['region'],
            json['space_time_info'],
            timetableFromJson,
            this.createTimetableThumb(timetableFromJson),
            json['title'],
            json['uid'],
            json['updated_at'],         
        )
        return eventFromJson;
    }
    
    /**
     * Concatenate the fields title, description and freeText for the keyword research
     */
    public getTextContent(): string {
        let textContent: string;
        textContent = `${this.title} ${this.description} ${this.freeText}`;
        return textContent;
    }

    /**
     * Check if the event last one day or more (one day = true)
     */
    public isOneDay(): boolean {
        return this.dateStart.getTime() - this.dateEnd.getTime() === 0;
    }

}