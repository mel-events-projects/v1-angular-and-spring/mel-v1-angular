import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { UserApiService } from 'src/app/services/user-api.service';
import { NewUser } from 'src/app/models/user.model';

@Component({
  selector: 'cdc-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent implements OnInit {

  private registerForm: FormGroup;
  private passwordForm: FormGroup;
  private emailForm: FormGroup;
  private usernameCtrl: FormControl;
  private passwordCtrl: FormControl;
  private confirmPasswordCtrl: FormControl;
  private emailCtrl: FormControl;
  private confirmEmailCtrl: FormControl;
  private creationError: boolean;

  static passwordMatch(group: FormGroup) {
    const password = group.get('password').value;
    const confirm = group. get('confirmPassword').value;
    return password == confirm ? null : { matchingError: true };
  }

  static emailMatch(group: FormGroup) {
    const email = group.get('email').value;
    const confirm = group. get('confirmEmail').value;
    return email == confirm ? null : { matchingError: true };
  }

  constructor(private fb: FormBuilder, private userApi: UserApiService, private router: Router) {
    this.usernameCtrl = fb.control('', Validators.required);
    this.emailCtrl = fb.control('', [Validators.required, Validators.email]);
    this.passwordCtrl= fb.control('', Validators.required);
    
    this.passwordForm = fb.group(
      { password: this.passwordCtrl, confirmPassword: this.confirmPasswordCtrl },
      { validator: RegisterFormComponent.passwordMatch }
    )

    this.emailForm = fb.group(
      { email: this.emailCtrl, confirmEmail: this.confirmEmailCtrl },
      { validator: RegisterFormComponent.emailMatch }
    )

    this.registerForm = fb.group({
      username: this.usernameCtrl,
      emailForm: this.emailForm,
      passwordForm: this.passwordForm  
    })

  }

  ngOnInit() {
    this.creationError = false;
  }

  register() {
    console.log(this.registerForm.value);
    console.log(this.registerForm.value.username);
    console.log(this.emailForm.value.email);
    console.log(this.passwordForm.value.password);
    const newUser = new NewUser(this.registerForm.value.username, this.emailForm.value.email, this.passwordForm.value.password);
    console.log(`${newUser.username} dans register-form.component`);
    this.userApi.registerUser(newUser).subscribe(
      result => {
        this.creationError = false;
        this.router.navigate(['']);
      }, err => {
        this.creationError = true;
        console.log(err);
      }
    );
  }

  cancel() {
    this.router.navigate(['/login']);
  }

}
